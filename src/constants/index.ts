import red from '../assets/red.png'
import blue from '../assets/blue.png'
import yellow from '../assets/yellow.png'
import green from '../assets/green.png'
import orange from '../assets/orange.png'

export const tubesCount = 7 // Количество пробирок
export const filledTubesCount = 5 // Количество пробирок с мячами
export const ballSize = 50 // Диаметр мяча
export const ballMargin = 5 // Расстояние от края мяча до стенки пробирки
export const ballsInTubeCount = 8 // Количество мячей в пробирке
export const tubesMargin = 30 // Расстояние от края пробирки до края поля или до следующей пробирки

export const tubeWidth = ballMargin*2+ballSize // Ширина пробирки
export const tubeHeight = (ballSize+ballMargin)*ballsInTubeCount+ballMargin // Высота пробирки

export const fieldWidth = (tubeWidth+tubesMargin)*tubesCount+tubesMargin // Ширина поля
export const fieldHeight = tubeHeight+tubesMargin*2 // Высота поля

export const colorsCount = 5 // Количество цветов
export const colors: string[] = ['', red, blue, yellow, green, orange] 

export const selectedBallTopPosition = -80
