export interface iID {
    tube: number
    ball: number
    colorNumber: number
}

export interface iGoTo {
    status: boolean // true если мячу selectedBall команда лететь в выбранную пробирку
    top: number
    left: number
    tube: number // в какую пробирку
    ball: number // в какую позицию
}

export type tGameState = 'init' | 'idle' | 'start' | 'progress' | 'done'
