/* Icons from https://icons8.com/icon/107458/sphere */

import { Provider } from 'react-redux'

import { Layout } from './components/layout/Layout'
import { store } from './store/store'

import './App.css'

// import reactLogo from './assets/react.svg'
// import viteLogo from '/vite.svg'

function App() {
    return (
        <Provider store={store}>
            <Layout />
        </Provider>
    )
}

export default App

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
