import { colors } from "../constants"
import { iID } from "../constants/interfaces";

export const getRandomColor = (min: number, max: number): number => {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1) + min); // The maximum is inclusive and the minimum is inclusive
}

export const getColorByNumber = (color: number): string => {
    if (color >=0 && color < colors.length) {
        return colors[color]
    }

    return ''
}

/**
 * Раскладываем id на tube, ball и color
 */
export const extractId = (id: string): iID => {
    const extractedId: string[] = id.split('-')
    return { tube: Number(extractedId[0]), ball: Number(extractedId[1]), colorNumber: Number(extractedId[2]) }
}

/**
 * Проверяем, есть ли место в пробирке. true - да, false - нет
 */
export const isSpaceInTube = (tube: number[]): boolean => {
    if (tube.indexOf(0) === -1) {
        return false
    }
    return true
}

/**
 * Находим нижний пустой элемент в пробирке, иначе -1
 */
export const getFirstEmpty = (tube: number[]): number => {
    for (let i=tube.length-1; i>=0; i--) {
        if (tube[i] === 0) return i
    }
    return -1
}

/**
 * Проверяем пробирку на заполненность одним цветом
 * true - заполнена одним цветом, false - нет
 */
export const isTubeFull = (tube: number[]): boolean => {
    const elem = tube[0]
    if (elem === 0) return false
    for (let i=1; i<tube.length; i++) {
        if (tube[i] !== elem) return false
    }
    return true
}

/**
 * Проверка на завершение игры
 */
export const checkCompleted = (tubes: number[][]): boolean => {
    for(let i=0; i<tubes.length; i++) {
        const elem = tubes[i][0]
        for(let j=1; j<tubes[i].length; j++) {
            if (tubes[i][j] !== elem) {
                return false
            }
        }
    }
    return true
}
