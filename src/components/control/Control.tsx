import { startGame, gameActive as makeGameActive, initField } from '../../store/actions'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { Timer } from './components/timer/Timer'

import styles from './Control.module.css'

export const Control = () => {
    const gameState = useAppSelector(store => store.app['gameState'])
    const dispatch = useAppDispatch()

    const handleClick = () => {
        if (gameState !== 'progress') {
            if (gameState === 'done') {
                dispatch(initField())
            } else {
                dispatch(startGame())
                setTimeout(() => {
                    dispatch(makeGameActive())
                }, 900)
            }
        }
    }

    return (
        <div className={styles.container} onClick={handleClick}>
            {gameState !== 'progress' && gameState !== 'done' ? <span>Start</span> : <Timer />}
        </div>
    )
}
