import { useEffect } from 'react'
import { incrementTimer } from '../../../../store/actions'
import { useAppDispatch, useAppSelector } from '../../../../store/hooks'

import styles from './Timer.module.css'

export const Timer = () => {
    const time = useAppSelector(store => store.app['time'])
    const gameDone = useAppSelector(store => store.app['gameDone'])
    const dispatch = useAppDispatch()

    useEffect(() => {
        let interval: number | undefined
        if (!gameDone) {
            interval = setInterval( () => {
                dispatch(incrementTimer())
            }, 1000)
        }

        return () => clearInterval(interval)
    }, [dispatch, gameDone])

    return (
        <div className={styles.container}>
            {time}
        </div>
    )
}
