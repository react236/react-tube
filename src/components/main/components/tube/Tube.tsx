import { useAppDispatch, useAppSelector } from '../../../../store/hooks'
import { Ball } from '../ball/Ball'

import { tubeHeight, tubeWidth, tubesMargin, ballSize, ballMargin } from '../../../../constants'
import { getColorByNumber, isSpaceInTube, getFirstEmpty, isTubeFull } from '../../../../utils'
import { goToTube } from '../../../../store/actions'

import styles from './Tube.module.css'

interface iProps {
    balls: number[] // Массив с мячами
    tubeNumber: number // Номер пробирки
}

export const Tube: React.FC<iProps> = ({ balls, tubeNumber }) => {
    const selectedBall = useAppSelector(store => store.app['selectedBall']) // Fix typescript error
    const dispatch = useAppDispatch()
    const tubes = useAppSelector(store => store.app['tubes']) // Fix typescript error

    
    const handleTubeClick = (e: any) => {
        if (selectedBall !== '' && isSpaceInTube(balls)) {
            const ball = document.querySelector(`div[data-id='${selectedBall}']`) // Ищем standalone мяч

            if (ball !== null) {
                // Вычисляем позицию для перемещения мяча на новое место
                const destinationTube = Number(e.currentTarget.dataset.tubeid)
                const destinationBall = getFirstEmpty(tubes[destinationTube])
                const top = tubesMargin + ballMargin + (ballSize+ballMargin)*destinationBall
                const left = (tubesMargin+tubeWidth)*destinationTube + tubesMargin + ballMargin

                // Команда на перемещение на новое место
                dispatch(goToTube({ status: true, top, left, tube: destinationTube, ball: destinationBall }))
            }
        }
    }

    const determineBoxShadow = (balls: number[]): string => {
        if (isSpaceInTube(balls)) return '5px 5px 5px cornflowerblue, -5px 5px 5px cornflowerblue'
        if (isTubeFull(balls)) return '5px 5px 5px yellowgreen, -5px 5px 5px yellowgreen'
        return 'none'
    }

    return (
        <div
            className={styles.container}
            style={{
                width: `${tubeWidth}px`,
                height: `${tubeHeight}px`,
                top: `${tubesMargin}px`,
                left: `${(tubesMargin+tubeWidth)*tubeNumber+tubesMargin}px`,
                boxShadow: determineBoxShadow(balls),
            }}
            data-tubeid={tubeNumber}
            onClick={handleTubeClick}
        >
            {balls.map( (item: number, index: number) => {
                // ballId -> 'tube-ball-colorNumber'
                const ballId = `${tubeNumber}-${index}-${item}`

                return (
                    <Ball key={index} id={ballId} color={getColorByNumber(item)} ballNumber={index} type='' />
                )
            })}
        </div>
    )
}

