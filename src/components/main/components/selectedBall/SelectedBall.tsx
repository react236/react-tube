
import { useAppSelector } from '../../../../store/hooks'
import { Ball } from '../../components/ball/Ball'

import { ballMargin, selectedBallTopPosition, tubesMargin, tubeWidth } from '../../../../constants'
import { extractId, getColorByNumber } from '../../../../utils'

export const SelectedBall = () => {
    const selectedBall = useAppSelector(store => store.app['selectedBall'])

    if (selectedBall) {
        const { tube, colorNumber } = extractId(selectedBall)
        // Рассчет положения относительно компонента Main
        const top = selectedBallTopPosition+30+1
        const left = (tubeWidth+tubesMargin)*tube+tubesMargin+ballMargin+1

        return (
            <Ball id={selectedBall} type='standalone' top={top} left={left} color={getColorByNumber(colorNumber)} />
        )
    }

    return null
}
