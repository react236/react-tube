import { useEffect, useRef } from 'react'
import confetti from 'canvas-confetti'
import { useAppDispatch, useAppSelector } from '../../../../store/hooks'
import { checkCompleted, extractId } from '../../../../utils'

import { ballMargin, ballSize, selectedBallTopPosition } from '../../../../constants'
import styles from './Ball.module.css'
import { addBallToTube, removeBallFromTube, selectBall, goToTube as goToTubeAction, gameDone as makeGameDone, } from '../../../../store/actions'
import { tGameState } from '../../../../constants/interfaces'

interface iProps {
    id: string
    ballNumber?: number
    color: string
    type: '' | 'standalone'
    top?: number // для standalone мяча
    left?: number // для standalone мяча
}

export const Ball: React.FC<iProps> = ({ id, ballNumber=0, color, type, top, left }) => {
    const ballRef = useRef<HTMLDivElement>(null)
    const saveTop = useRef<string>('0')
    const selectedBall = useAppSelector(store => store.app['selectedBall']) // Fix typescript error
    const tubes = useAppSelector(store => store.app['tubes']) // Fix typescript error
    const goToTube = useAppSelector(store => store.app['goToTube']) // Fix typescript error
    const gameState = useAppSelector(store => store.app['gameState'])
    const dispatch = useAppDispatch()

    useEffect(() => {
        if (type === 'standalone') {
            const elem = ballRef.current

            if (elem) {
                if (goToTube['status']) {
                    elem.style.top = `${goToTube['top']}px`
                    elem.style.left = `${goToTube['left']}px`
                    elem.classList.add(styles.animateIn)
                } else {
                    elem.classList.remove(styles.animateIn)
                }
            }
        }
    }, [id, type, selectedBall, goToTube])

    /**
     * Проверяем, есть ли еще мячи над указанным
     */
    const checkTop = (id: string): boolean => {
        const { tube, ball } = extractId(id)

        if (ball > 0 && tubes[tube][ball-1] > 0) {
            return false
        }
        return true
    }

    const handleBallClick = (e: any) => {
        // Если мяч уже выбран, другой нельзя выбрать
        if (selectedBall === '') {
            e.stopPropagation()
            const id = e.target.dataset.id
            const isTop = checkTop(id)

            if (id && isTop) {
                saveTop.current = e.target.style.top // Сохраняем вертикальную позицию
                e.target.classList.add(styles.animateOut)
                e.target.classList.remove(styles.canSelectBall)
                e.target.style.top = `${selectedBallTopPosition}px`
            }
        }
    }

    const handleAnimationEnd = (e: any) => {
        if (type === 'standalone') {
            if (e.propertyName === 'top') { // CSS свойство завершенной анимации
                e.target.classList.remove(styles.animateIn) // Убираем анимацию из CSS
                dispatch(addBallToTube()) // Ставим мяч на новую позицию
                dispatch(goToTubeAction({ status: false, top: 0, left: 0, tube: 0, ball: 0 })) // Снимаем команду перемещения на новое место
                dispatch(selectBall('')) // Удаляем standalone мяч
                if (checkCompleted(tubes)) {
                    dispatch(makeGameDone()) // Игра окончена
                    confetti({
                        origin: { x: 0.5, y: 0.8 }
                    })
                } else {
                    e.target.classList.add(styles.canSelectBall) // Восстанавливаем класс для выбора мяча
                }
            }
        } else if (gameState !== 'start') {
            e.target.classList.remove(styles.animateOut)
            dispatch(selectBall(id)) // Делаем выбранный мяч 'standalone'
            dispatch(removeBallFromTube(id)) // Убираем его из пробирки
            e.target.style.top = saveTop.current // Восстанавливаем вертикальную позицию, смещенную после анимации выбора мяча
        }
        if (gameState === 'start') {
            e.target.style.transition = ''
        }
    }

    // Показываем стрелку при наведении на мяч
    const handleBallHover = (e: any) => {
        if (selectedBall === '' && color !== '') {
            if (e.type === 'mouseenter') {
                e.target.classList.add(styles.canSelectBall)
            } else {
                e.target.classList.remove(styles.canSelectBall)
            }
        }
    }

    // Определяем top позицию мяча
    const determineTop = (
        type: string, 
        top: number | undefined, 
        ballNumber: number, 
        gameState: tGameState
    ): string => {
        if (type === 'standalone') return `${top}px`
        if (gameState === 'idle' || gameState === 'init') return `${selectedBallTopPosition}px`
        return `${(ballSize+ballMargin)*ballNumber+ballMargin}px`
    }

    // Определяем left позицию мяча
    const determineLeft = (type: string, left: number | undefined): string => {
        if (type === 'standalone') return `${left}px`
        return `${ballMargin}px`
    }

    return (
        <div
            ref={ballRef}
            className={styles.container}
            style={{ 
                background: `url(${color})`,
                top: determineTop(type, top, ballNumber, gameState),
                left: determineLeft(type, left),
                width: ballSize+'px',
                height: ballSize+'px',
                transition: (gameState === 'idle' || gameState === 'start') ? `top ${(ballNumber+1)*0.1}s` : '' // Transition для начальной анимации
            }}
            data-id={id}
            onClick={handleBallClick}
            onTransitionEnd={handleAnimationEnd}
            onMouseEnter={handleBallHover}
            onMouseLeave={handleBallHover}
        ></div>
    )
}
