import { useAppSelector } from '../../store/hooks'
import { Tube } from './components/tube/Tube'
import { SelectedBall } from './components/selectedBall/SelectedBall'

import { fieldHeight, fieldWidth } from '../../constants'
import styles from './Main.module.css'

export const Main = () => {
    const tubes = useAppSelector(store => store.app['tubes'])

    return (
        <div 
            className={styles.container}
            style={{
                width: `${fieldWidth}px`,
                height: `${fieldHeight}px`
            }}
        >
            {tubes.map( (item, index) => (
                <Tube key={index} balls={item} tubeNumber={index} />
            ))}
            <SelectedBall />
        </div>
    )
}
