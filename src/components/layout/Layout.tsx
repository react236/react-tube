import { useEffect } from 'react'
import { useAppDispatch } from '../../store/hooks'

import { Main } from '../main/Main'
import { Control } from '../control/Control'

import styles from './Layout.module.css'

export const Layout = () => {
    const dispatch = useAppDispatch()

    useEffect(() => {
        // dispatch(initField())
    })

    return (
        <div className={styles.container}>
            <Main />
            <Control />
        </div>
    )
}
