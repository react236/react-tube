import { AnyAction } from 'redux'
import { iGoTo, tGameState } from '../constants/interfaces'

import {
    ADD_BALL_TO_TUBE,
    GAME_ACTIVE,
    GAME_DONE,
    GO_TO_TUBE,
    INCREMENT_TIMER, 
    INIT_FIELD,
    REMOVE_BALL_FROM_TUBE,
    SELECT_BALL,
    SET_GAME_STATE,
    START_GAME,
} from './constants'

export function initField(): AnyAction {
    // store.dispatch(initMinesLeft())
    // store.dispatch(setGameWon(false))
    return { type: INIT_FIELD }
}

export function selectBall(id: string): AnyAction {
    return { type: SELECT_BALL, payload: id }
}

export function removeBallFromTube(id: string): AnyAction {
    return { type: REMOVE_BALL_FROM_TUBE, payload: id }
}

export function goToTube(action: iGoTo): AnyAction {
    return { type: GO_TO_TUBE, payload: action }
}

export function addBallToTube(): AnyAction {
    return { type: ADD_BALL_TO_TUBE }
}

export function startGame(): AnyAction {
    return { type: START_GAME }
}

export function gameActive(): AnyAction {
    return { type: GAME_ACTIVE }
}

export function incrementTimer(): AnyAction {
    return { type: INCREMENT_TIMER }
}

export function gameDone(): AnyAction {
    return { type: GAME_DONE }
}

export function setGameState(gameState: tGameState): AnyAction {
    return { type: SET_GAME_STATE, payload: gameState }
}
