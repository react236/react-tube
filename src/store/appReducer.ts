import { AnyAction } from "redux"

import { extractId, getRandomColor } from '../utils'

import {
    ADD_BALL_TO_TUBE,
    GAME_ACTIVE,
    GAME_DONE,
    GO_TO_TUBE,
    INCREMENT_TIMER,
    INIT_FIELD,
    REMOVE_BALL_FROM_TUBE,
    SELECT_BALL,
    SET_GAME_STATE,
    START_GAME,
} from './constants'

import { ballsInTubeCount, colorsCount, filledTubesCount, tubesCount } from '../constants'
import { iGoTo, tGameState } from "../constants/interfaces"

// Состояние игры:
// init инициализация
// idle (store инициализирован, начальный экран)
// start (стартовая анимация мячей)
// progress (игра активна)
// done (фейерверк, остановка таймера)

interface iState {
    tubes: number[][]
    time: number // Время в секундах
    selectedBall: string // ID выбранного мяча
    goToTube: iGoTo // Реализация команды перемещения мяча на новую позицию
    gameState: tGameState // Cостояние игры
}

const initField = (): number[][] => {
    // Создаем массивы - пробирки
    const tubes: number[][] = new Array(tubesCount)
    for(let i=0; i<colorsCount; i++) {
        tubes[i] = new Array(ballsInTubeCount)
    }
    // Заполняем пустые пробирки
    for(let i=filledTubesCount; i<tubesCount; i++) {
        tubes[i] = new Array(ballsInTubeCount).fill(0)
    }

    // Массив с количеством шаров каждого цвета
    const colorsCountsArray = new Array(colorsCount).fill(0)

    // Заполняем пробирки цветами
    let tube = 0, ball = 0
    const counter = filledTubesCount * ballsInTubeCount
    let i = 0
    while (i <= counter-1) {
        const color = getRandomColor(1, colorsCount)
        if (colorsCountsArray[color-1] < ballsInTubeCount) {
            tubes[tube][ball] = color
            colorsCountsArray[color-1] += 1
            ball += 1
            if (ball >= ballsInTubeCount) {
                ball = 0
                tube += 1
            }
            i += 1
        }
    }

    return tubes
}

const initialState: iState = {
    tubes: initField(),
    time: 0,
    selectedBall: '',
    goToTube: {
        status: false,
        top: 0,
        left: 0,
        tube: 0,
        ball: 0,
    },
    gameState: 'idle',
}


export const appReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        // Инициализация игры
        case INIT_FIELD: { 
            const newState = Object.assign({}, initialState)
            newState.tubes = initField()
            //newState.gameDone = true
            newState.gameState = 'init'

            return newState
        }
        // Устанавливаем выбранный мяч
        case SELECT_BALL: { return { ...state, selectedBall: action.payload } }
        // Удаляем мяч из пробирки
        case REMOVE_BALL_FROM_TUBE: {
            const { tube, ball } = extractId(action.payload)
            const newTubes: number[][] = Object.assign([], state.tubes)
            newTubes[tube][ball] = 0
            return { ...state, tubes: newTubes }
        }
        // Команда мячу лететь в пробирку
        case GO_TO_TUBE: {
            return { ...state, goToTube: Object.assign({}, action.payload) }
        }
        // Добавляем мяч в пробирку
        case ADD_BALL_TO_TUBE: {
            const { colorNumber } = extractId(state.selectedBall)
            const newTubes: number[][] = Object.assign([], state.tubes)
            newTubes[state.goToTube.tube][state.goToTube.ball] = colorNumber
            return { ...state, tubes: newTubes }
        }
        // Нажата кнопка Start
        case START_GAME: {
            return { ...state, gameState: 'start' }
        }   
        // Запуск игры
        case GAME_ACTIVE: {
            return { ...state, gameState: 'progress' }
        }
        // Увеличиваем таймер
        case INCREMENT_TIMER: {
            return { ...state, time: state.time+1 }
        }
        // Игра окончена
        case GAME_DONE: {
            return { ...state, gameState: 'done' }
        }
        // Установка состояния игры
        case SET_GAME_STATE: {
            return { ...state, gameState: action.payload }
        }
        default: { return state }
    }
}
